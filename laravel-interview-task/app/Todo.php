<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    public $timestamps = false;


    protected $fillable = [
        'title', /*'user_id',*/ 'deleted', 'created_at', 'updated_at'
    ];

}
