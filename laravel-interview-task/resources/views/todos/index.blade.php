@extends('todos.layout')

@section('content')
    <div class="row" style="display: flow-root">
        <div class="float-left">
            <h2 class="float-left">Todo CRUD</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-success float-right" href="{{ route('todos.create') }}"> Add Todo</a>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>User</th>
            <th>Date</th>
            <th style="width: 300px">Action</th>
        </tr>
        @foreach ($todos as $todo)
            <tr>
                <td>{{ $todo->id }}</td>
                <td>{{ $todo->title }}</td>
                <td>{{ $todo->user_id }}</td>
                <td>{{ $todo->created_at }}</td>
                <td>
                    <form action="{{ route('todos.destroy',$todo->id) }}" method="POST">
                        <a class="btn btn-info" href="{{ route('todos.show',$todo->id) }}">Show</a>
                        <a class="btn btn-primary" href="{{ route('todos.edit',$todo->id) }}">Edit</a>

                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $todos->links() !!}

@endsection
