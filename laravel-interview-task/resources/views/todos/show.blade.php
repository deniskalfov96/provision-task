@extends('todos.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2> Show Todo item</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="{{ route('todos.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <strong>Title:</strong>
                {{ $todo->title }}
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>User:</strong>
                {{ $todo->user_id }}
            </div>
        </div>
    </div>
@endsection
